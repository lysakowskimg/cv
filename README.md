# Curriculum Vitae de LYSAKOWSKI Mathieu

Vous trouverez ci-joint mon Curriculum Vitae.

Celui-ci est disponible à l'adresse suivante [https://lysakowskimg.gitlab.io/cv/](https://lysakowskimg.gitlab.io/cv/).

Vous pouvez également télécharger mon Curriculum Vitae au [format PDF](https://lysakowskimg.gitlab.io/cv/cv.pdf).

Vous trouverez la version compléte de mon Curriculum Vitae à l'adresse suivante [https://lysakowskimg.gitlab.io/cv/full/](https://lysakowskimg.gitlab.io/cv/full/) ainsi que son équivalent au [format PDF](https://lysakowskimg.gitlab.io/cv/cv_full.pdf).

Ce site utilise [Hugo](https://gohugo.io/) ainsi que le thème [Hugo DevResume](https://github.com/cowboysmall-tools/hugo-devresume-theme).